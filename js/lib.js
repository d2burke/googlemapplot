$(document).ready(function(){

});

Number.prototype.toRad = function() {
   return this * Math.PI / 180;
}

//RED PATH	
var path1 = JSON.parse('[[36.84638, -76.28380],[36.84672, -76.28464],[36.84703, -76.28544],[36.84746, -76.28657],[36.84767, -76.28772],[36.84773, -76.28929],[36.84781, -76.29033],[36.84802, -76.29149],[36.84813, -76.29259],[36.84822, -76.29308]]');      

//BLUE PATH
var path2 = JSON.parse('[[36.85270, -76.28877],[36.85264, -76.28913],[36.85182, -76.28956],[36.85106, -76.28992],[36.85044, -76.29026],[36.84952, -76.29072],[36.84898, -76.29099],[36.84803, -76.29147],[36.84694, -76.29205],[36.84636, -76.29234]]');

var map;
var n = 0;
var timeoutID;
var phone_screen;



/*
 *	INSTANTIATE INITIAL GOOGLE MAP
 *	SET VARIOUS OPTIONS
 *	ADD MAP TO THE DESIGNATED DIV
 */

function initialize(){
	//CREATE AND INITIALIZE NEW MAP
	var start = new google.maps.LatLng(36.85036, -76.28865);
	var mapOptions = {
		zoom: 16,
		center: start,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
}


/*
 *	ITERATE OVER path1 POINTS
 *	CREATE NEW LatLng OBJECT
 *	SET CUSTOM ICON (POINT IMAGE)
 *	PLOT MARKER
 */
function addPoint(){
	
	var bound = new google.maps.LatLngBounds();
	
	//DO STUFFS
	if(n < path1.length){
	
		//EXTEND THE MAP PLOT AREA
		var new_point = new google.maps.LatLng(path1[n][0], path1[n][1]);
		bound.extend( new_point );
		
		//ADD MARKER AT EACH POINT
		var marker = new google.maps.Marker();
		marker.setPosition(new_point);
		marker.setIcon("images/car1.png");
		marker.setMap(map);
	}
	
	if(n < path2.length){
	
		//EXTEND THE MAP PLOT AREA
		var new_point = new google.maps.LatLng(path2[n][0], path2[n][1]);
		bound.extend( new_point );
					
		//ADD MARKER AT EACH POINT
		var marker = new google.maps.Marker();
		marker.setPosition(new_point);
		marker.setIcon("images/car2.png");
		marker.setMap(map);
	}
	
	
	if(path1[n] && path2[n]){
		map.panTo(bound.getCenter());
		isBump(path1[n], path2[n]);
	}
	
	n++;
}

function isBump(carPoint1, carPoint2){
	
	var lat1 = carPoint1[0]; 
	var lon1 = carPoint1[1]; 
	
	var lat2 = carPoint2[0]; 
	var lon2 = carPoint2[1];
	
	var R = 6371; // km 
	//has a problem with the .toRad() method below.
	var x1 = lat2-lat1;
	var dLat = x1.toRad();  
	var x2 = lon2-lon1;
	var dLon = x2.toRad();  
	var a = Math.sin(dLat/2) * Math.sin(dLat/2) + 
	                Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * 
	                Math.sin(dLon/2) * Math.sin(dLon/2);  
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	var d = R * c; 
	var dFeet = d * 3280.84;
	
	if(dFeet < 15){
		var bump_point1 = new google.maps.LatLng(lat1, lon1);
		var bump_point2 = new google.maps.LatLng(lat2, lon2);
	
		var bump_bound = new google.maps.LatLngBounds();
		bump_bound.extend( bump_point1);
		bump_bound.extend( bump_point2 );
		
		//ADD MARKER AT EACH POINT
		var marker = new google.maps.Marker();
		marker.setPosition(bump_bound.getCenter());
		marker.setIcon("images/bump.png");
		marker.setMap(map);
	}
}

function plotPath(){
	clearPath();
	timeoutID = window.setInterval(addPoint, 1000);
}

function hideMarkers(map, locations, markers) {
    /* Remove All Markers */
    while(markers.length){
        markers.pop().setMap(null);
    }

    console.log("Remove All Markers");
}

function clearPath(){
	n = 0;
	clearInterval(timeoutID);
}